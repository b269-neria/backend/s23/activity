let trainer = {
    name: "Ash Ketchum",
    age: 13,
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock","Misty"]
    },
    pokemon: ["Pikachu", "Charizard", "Squirtle","Bulbasaur"],
    talk: function(){
        console.log("Pikachu! I choose you!");
    }
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
trainer.talk();

function Pokemon(name,level){
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
    this.faint = function(){
        console.log(this.name+ " has fainted.");
    }

    this.tackle = function(enemy){
        console.log(this.name + " used tackle on "+ enemy.name);
        enemy.health = enemy.health - this.attack;
        console.log(enemy.name+"'s health has been reduced to "+enemy.health);
        if(enemy.health <= 0){
            enemy.faint();
            console.log(enemy);
        }
    }

    
}

let pikachu = new Pokemon("Pikachu", 40);
let lapras = new Pokemon("Lapras", 45);
let kyogre = new Pokemon("Kyogre", 81);
console.log(pikachu);
console.log(lapras);
console.log(kyogre);

pikachu.tackle(lapras);
lapras.tackle(pikachu);
kyogre.tackle(pikachu);
kyogre.tackle(lapras);